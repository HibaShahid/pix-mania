import * as React from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Home from './HomeComponent';
import PhotoBook from './PhotoBookComponent';
import PhotoBookDetails from './PhotoBookDetailComponent';
import SelectImage from './SelectImageComponent';
import SpecificationComponent from './SpecificationComponent';
import SignInComponent from './SignInComponent';
import CreateAccount from './CreateAccountComponent';

function CustomHeader({title, isHome, navigation}) {
    return (
        <View style={{flexDirection:'row', height:50}}>
            <View style={{flex:1,justifyContent: 'center' }}>
                {
                    isHome ? null :
                    <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => navigation.goBack()}
                    >
                    <Image style={{width: 20, height: 20, marginLeft: 10}}
                        source={require('./images/return.png')}
                        resizeMode="contain"
                    />
                         <Text>Back</Text>
                    </TouchableOpacity>
                }
            </View>
            <View style={{flex:1.5,justifyContent: 'center',}}>
                <Text style={{textAlign:'center'}}>{title}</Text>
            </View>
            <View style={{flex:1,justifyContent: 'center', alignItems:'flex-end', marginRight: 10}}>
            <Image style={{width: 30, height: 30,}}
                source={require('./images/login.png')}
                resizeMode='contain'
                />
            </View>
        </View>
    )
}

function Cart({navigation}){
    return(
        <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader title= "Cart" isHome={true} navigation={navigation}/>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Cart!</Text>
        <TouchableOpacity style={{marginTop: 10}}
        onPress={() => alert('poopo')}
        >
            <Text>Go Home Detail</Text>
        </TouchableOpacity>
        </View>
       
      </SafeAreaView>
    )
}
function HomeScreen({navigation}) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <CustomHeader title= "Home" isHome={true} navigation={navigation}/>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Home!</Text>
      <TouchableOpacity style={{marginTop: 10}}
      onPress={() => navigation.navigate('HomeDetail')}
      >
          <Text>Go Home Detail</Text>
      </TouchableOpacity>
      </View>
     
    </SafeAreaView>
  );
}

function HomeScreenDetail({navigation}) {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader title= "Home Detail"  navigation={navigation}/>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Home Detail!</Text>
        </View>
       
      </SafeAreaView>
    );
  }

function SettingsScreen({navigation}) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <CustomHeader title= "Settings" isHome={true} />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Setting!</Text>
      <TouchableOpacity style={{marginTop: 10}}
      onPress={() => navigation.navigate('SettingDetail')}
      >
          <Text>Go Settings Detail</Text>
      </TouchableOpacity>
      </View>
     
    </SafeAreaView>
  );
}

function SettingsScreenDetail({navigation}) {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeader title= "Settings Detail" navigation={navigation} />
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Setting Detail!</Text>
        </View>
       
      </SafeAreaView>
    );
  }

  function NotificationsScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button onPress={() => navigation.goBack()} title="Go back home" />
      </View>
    );
  }
  const Drawer = createDrawerNavigator();

const Tab = createBottomTabNavigator();

const navOptionHandler = () => ({
    headerShown: false
})

const StackHome = createStackNavigator();

function HomeStack() {
    return (
        <StackHome.Navigator initialRouteName= 'Home'>
            <StackHome.Screen name= 'Home' component= {Home} options={navOptionHandler}/>
            <StackHome.Screen name= 'PhotoBook' component= {PhotoBook} options={navOptionHandler}/>
            <StackHome.Screen name= 'PhotoBookDetails' component= {PhotoBookDetails} options={navOptionHandler}/>
            <StackHome.Screen name= 'SelectImage' component= {SelectImage} options={navOptionHandler}/>
            <StackHome.Screen name= 'SpecificationComponent' component= {SpecificationComponent} options={navOptionHandler}/>
        </StackHome.Navigator>
    )
}

const StackSetting = createStackNavigator();

function SettingStack() {
    return (
        <StackSetting.Navigator initialRouteName= 'Setting'>
            <StackSetting.Screen name= 'Setting' component= {SettingsScreen} options={navOptionHandler}/>
            <StackSetting.Screen name= 'SettingDetail' component= {SettingsScreenDetail} options={navOptionHandler}/>
        </StackSetting.Navigator>
    )
}

export default function MyNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
              screenOptions={({ route }) => ({
                tabBarIcon: ( ) => {
                  let iconName;
      
                  if (route.name === 'Home') {
                    iconName = require('./images/home-run.png');
                  } else if (route.name === 'Settings') {
                    iconName = require('./images/strategic-plan.png');
                  }else if (route.name === 'Cart') {
                    iconName = require('./images/shopping-cart.png');
                  }
      
                  // You can return any component that you like here!
                  return <Image source={iconName} style={{width:20, height:20}} resizeMode="contain"/>;
                },
              })}
              tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'black',
              }}
      >
        <Tab.Screen name="Home" component={HomeStack} />
        <Tab.Screen name="Settings" component={PhotoBook} />
        <Tab.Screen name="Cart" component={Cart} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
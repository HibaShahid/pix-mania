import React, {Component, useState} from 'react';
import {StyleSheet, View, Text, FlatList, TouchableOpacity, Alert} from 'react-native';
import { Card, ListItem, Button, Icon, Tile, Overlay  } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import CustomHeader from './CustomHeaderComponent';

const mapStateToProps = state => {
    return {
      categories: state.categories
    }
  }

  var flatCol = 2;
  class Home extends Component {

    render() {
        
        const renderCategoryItem = ({item, index}) => {
            return(
              
              <TouchableOpacity 
                style={styles.container}
                onPress={() => this.props.navigation.navigate('PhotoBookDetails')  }
                >
                <Card
                containerStyle={styles.card}
                key = {index}
                image = {{uri: baseUrl + item.image}}
      
                >
                <Text style={styles.cardText}>{item.name}</Text>
                </Card>
                </TouchableOpacity>
            );
          };
        return(
          
          <View style={styles.container}>
        <CustomHeader title= "Cart" isHome={true}/>
              <Tile
              style={styles.tilecontainer}
              imageSrc={require('./images/gifts.jpg')}
              title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores dolore exercitationem"
              featured
              caption="Some Caption Text"
              />
              <View style={styles.allheading}>
                <Text style={styles.allheadingtext}>Photo Cards</Text>
              </View>
      
            <FlatList
            numColumns = {flatCol} 
            data={this.props.categories.categories}
            renderItem={renderCategoryItem}
            keyExtractor={item => item.id.toString()}
            />
            </View>
        );
    }
}
export default connect(mapStateToProps)(Home);


const styles = StyleSheet.create({
container: {
  flex: 1,
  marginTop: 10,
  width: '100%',
  height: '50%',
  backgroundColor: '#fff',
},
tilecontainer: {
},
card : {
  borderWidth: 0,
  elevation: 0,
  shadowColor: 'rgba(0,0,0, .2)',
  shadowOffset: { height: 0, width: 0 },
  shadowOpacity: 0, //default is 1
  shadowRadius: 0

},
cardText: {
  textAlign: 'center'
 // borderColor: 'pink'
},
allheading:{
  marginTop: 10
},
allheadingtext:{
 fontSize: 25
}
})

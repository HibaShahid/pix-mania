import * as React from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image, TouchableOpacity } from 'react-native';


export default function CustomHeader({title, isHome, navigation}) {
    return (
        <View style={{flexDirection:'row', height:50}}>
            <View style={{flex:1,justifyContent: 'center' }}>
                {
                    isHome ? null :
                    <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => navigation.goBack()}
                    >
                    <Image style={{width: 20, height: 20, marginLeft: 10}}
                        source={require('./images/return.png')}
                        resizeMode="contain"
                    />
                         <Text>Back</Text>
                    </TouchableOpacity>
                }
            </View>
            <View style={{flex:1.5,justifyContent: 'center',}}>
                <Text style={{textAlign:'center'}}>{title}</Text>
            </View>
            <View style={{flex:1,justifyContent: 'center', alignItems:'flex-end', marginRight: 10}}>
            <Image style={{width: 30, height: 30,}}
                source={require('./images/login.png')}
                resizeMode='contain'
                />
            </View>
        </View>
    )
}
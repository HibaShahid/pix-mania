import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

 function CreateAccount({navigation}) {
    return(
        <View>
            <Text>CreateAccount</Text>
            <Button
                
                iconRight
                title="CreateAccount"
                onPress={() => navigation.goBack()}
                />
        </View>
    );
}

export default CreateAccount;
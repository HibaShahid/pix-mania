import React, {Component} from 'react';
import {StyleSheet, View, Text, Platform } from 'react-native';
import { connect } from 'react-redux';
import { fetchCategories } from '../redux/ActionCreators';
import { fetchFrames } from '../redux/ActionCreators';
import MyNavigation from './NavigationComponent';

const mapStateToProps = state => {
    return {
      categories: state.categories,
      frames: state.frames
    }
  }
  
  const mapDispatchToProps = dispatch => ({
    fetchCategories: () => dispatch(fetchCategories()),
    fetchFrames: () => dispatch(fetchFrames())
  })
  

class Main extends Component{

    componentDidMount() {
        this.props.fetchCategories();
        this.props.fetchFrames();
      }
      render() {
      
          return (
          
            <View style={styles.container}>
              <MyNavigation/>
            </View>    
            
      );
      
      
        }
  
  }
export default connect(mapStateToProps, mapDispatchToProps)(Main);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      ///////////paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight
     // alignItems: 'center',
      //justifyContent: 'center',
    },
    responsiveBox: {
      width: ('84.5%'),
      height: ('70%'),
      borderWidth: 2,
      borderColor: 'green',
      flexDirection: 'column',
      justifyContent: 'space-around' 
    }
    });
  
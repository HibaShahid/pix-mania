import React, {Component} from "react";
import {
  StyleSheet,
  View, ScrollView,
  Dimensions, Image,
  Text, TextInput
} from "react-native";
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Rating } from 'react-native-ratings';
import { Avatar, Badge, Icon,Button ,SocialIcon } from 'react-native-elements'

const mapStateToProps = state => {
  return {
    categories: state.categories
  }
}

const  width  = Dimensions.get("window").width;
    const height = width * 0.6; 
class PhotoBookDetails extends Component {
  state ={
    active: 0,
    showMe: false
  }
  opertation(){
    this.setState({
      showMe: !this.state.showMe
    })
  }
  change = ({nativeEvent}) => {
    const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.wigth);
    if(slide !== this.state.active){
      this.setState({active: slide});
    }  
  }
 
  render() {
  const item = this.props.categories.categories;
  return (
    <View style={{flex:1,backgroundColor: "#fff"}}>
    <ScrollView>
    <View style={styles.container}>
      <ScrollView 
      pagingEnabled 
      horizontal 
      onScroll={this.change}
      style={styles.scroll}
      showsHorizontalScrollIndicator={false}
      >
          {
            item.map((item,index) => (
              <Image
                key={index}
                source={{uri: baseUrl + item.image}}
                style = {styles.image}
              />
            ))
          } 

        </ScrollView>
        <View style={styles.pagination}>
        {
          item.map((i,k) => (
            <Text key = {k} style={k==this.state.active ? styles.pagingActiveText : styles.pagingText}>⬤</Text>
          )

          )
        }
        </View>
      </View>
      <View>
        <Text style={{ fontSize: 25, color: 'blue',padding:10, marginTop: 10}}>Standard Photo Book</Text>
        {/* This is for rating
        <Rating
               type='custom'
              ratingCount={5}
              imageSize={25}
              showRating
              ratingColor='black'
              ratingBackgroundColor='white'
              showRating={false}
              style={{alignSelf: 'flex-start' }}
             // starContainerStyle={{}}
              //onFinishRating={this.ratingCompleted}
        />*/}
            <Text style={{ padding: 10, fontSize: 20, textAlign: 'justify'}}>
          <Text>
            A Standard Photo Book is the perfect way to print your entire year, vacation, or special
            occasion. Fill a book with up to 366 pages with photos right from your phone.
          </Text>  
         
          </Text>
      </View>  
      <View>
        <Text style={{ padding: 10, fontSize: 20, color: 'green'}}>
        Starting at $10
          </Text>
      </View>  
      <View style={{ alignSelf: 'center' }}>
        <Text style={{ padding: 10, fontSize: 20, color: 'green', alignSelf:'center'}}>
          How It Works
          </Text>
          <Avatar
            size="large"
            rounded
            title="1"
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: 'blue'}}
            containerStyle={{ marginTop: 0, alignSelf:'center'}}
            />
          <Text style={{ padding: 10, fontSize: 20,alignSelf:'center'}}>
                Select Your Book Size{'\n'}and Cover Type
          </Text>
          <Avatar
            size="large"
            rounded
            title="2"
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: 'blue'}}
            containerStyle={{ marginTop: 0, alignSelf:'center'}}
            />
          <Text style={{ padding: 10, fontSize: 20,alignSelf:'center'}}>
                Choose Your Photo Source{'\n'} and Select Your Photos
          </Text>
          
          <Avatar
            size="large"
            rounded
            title="3"
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
            overlayContainerStyle={{backgroundColor: 'blue'}}
            containerStyle={{ marginTop: 0, alignSelf:'center'}}
            />
          <Text style={{ padding: 10, fontSize: 20,alignSelf:'center'}}>
                Customize and{'\n'} Send to Prints
          </Text>
          <View>
          <Image source={require("./images/sizes.jpeg")}
          style={{width,height:400}}/>
          </View>
          <View>
            <Text style={{ fontSize: 25, color: 'blue',padding:10, marginTop: 10, fontStyle:'italic'}}>
              A Book For Every Occasion
            </Text>
            <Text style={{ padding: 10, fontSize: 20, textAlign: 'justify'}}>
              <Text>
                Your favorite family trip, baby's first year, or a gift for grandma. Easily create a personalized present for everyone.
              </Text>  
            </Text>
            <Image source={require("./images/occasion.jpeg")}
          style={{width,height:400}}/>
          </View>
          <View>
            <Text style={{ fontSize: 25, color: 'blue',padding:10, marginTop: 10, fontStyle:'italic'}}>
              Amazing Quality
            </Text>
            <Text style={{ padding: 10, fontSize: 20, textAlign: 'justify'}}>
              <Text>
                Printed with archival quality ink and perfectly bound bindings using PUR adhesive for a strong, long-lasting finish. Available in navy, gray and white.
              </Text>  
            </Text>
            <Image source={require("./images/amazing_quality.jpg")}
          style={{width,height:400}}/>
          </View>
          <View>
            {
              this.state.showMe ? 
              <Text>
                hide me
              </Text>
              :null
            }
            <Button onPress={()=>this.opertation()} title='press me'/>
            
            </View>
      </View>  
      <Button
      
        title='Create' 
        onPress={() => this.props.navigation.navigate('SpecificationComponent') }
       
       />
      </ScrollView>
    </View>
  )
  }
}


const styles = StyleSheet.create({
  container: {
    //flex:1,
    marginTop: 0, 
    width,
     height,backgroundColor:'pink'
  },
  scroll: { 
    width,
     height
  },
    
  image: {
    width,
     height,
    resizeMode: 'cover'
  },
    pagination: {
    flexDirection:'row',
    position:'absolute',
    bottom:0,
    alignSelf:'center',
  },
  pagingText: {
    color:'#888',
    margin:3
  },
  pagingActiveText: {
    color:'#fff',
    margin:3,
    fontSize: (width / 30)
  }
});

export default connect(mapStateToProps)(PhotoBookDetails);

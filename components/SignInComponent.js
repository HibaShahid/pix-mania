import React, {Component} from 'react';
import {  View, StyleSheet, TouchableHighlight, TextInput, TouchableOpacity, Image} from 'react-native';
import { Text, Input, Icon, Button } from 'react-native-elements';


class SignInComponent extends Component {
    constructor () {
        super()
        this.state = {
           username: "",
           password: ""
        }
    }
    
    render(){
        
        return (
            <View style= {styles.container}>
            <Image
            source={require('./images/logo.png')}
             resizeMode= 'center'
            />
            <TextInput placeholder ={"Enter the user Name"}
            onChangeText={(value)=>{this.setState({username: value})}}
                style={{height: 42, width: "80%", borderBottomWidth: 1}} />
            <TextInput placeholder ={"Enter the Password"}
             onChangeText={(value)=>{this.setState({password: value})}}
                style={{height: 42, width: "80%", borderBottomWidth: 1, marginTop: '5%'}} />  
            <View style={{marginTop: '10%', width: '80%'}}>
                <TouchableOpacity style={{borderWidth: 1, height: 42, width: '80%', 
                justifyContent: 'center', alignItems: 'center', borderRadius: 40,
                backgroundColor: '#2E8B57', alignSelf: 'center', textAlign: 'center', marginBottom: 20}} >
                    <Text style={{color: 'white'}}>SignIn</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                 onPress={() => this.props.navigation.navigate('SignUpScreen')  }
                style={{borderWidth: 1, height: 42, width: '80%', 
                justifyContent: 'center', alignItems: 'center', borderRadius: 40,
                backgroundColor: '#2E8B57', alignSelf: 'center', textAlign: 'center'}} >
                    <Text style={{color: 'white'}}>SignUp</Text>
                </TouchableOpacity>
            </View>
             
            </View>

                )
             }
                    

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
     width: '100%',
     height: '100%',
      justifyContent: 'center',
      alignSelf: 'center',
      alignItems: 'center',
      alignContent: 'center',
      backgroundColor: 'white'
    },
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20
      },
      formLabel: {
          fontSize: 18,
          flex: 2
      },
      formItem: {
      },
      button: {
        alignItems: "center",
        //backgroundColor: "#DDDDDD",
        padding: 10,
       
        alignSelf: 'center'
      },
    })
    export default SignInComponent;
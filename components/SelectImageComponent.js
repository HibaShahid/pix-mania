import React, {Component} from "react";
import {
  StyleSheet,
  FlatList,
  View, ScrollView,
  Dimensions, Image,
  Text, TouchableOpacity, Modal
} from "react-native";
import { connect } from 'react-redux';
import { ActionSheet, Root, Card } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
const mapStateToProps = state => {
    return {
      categories: state.categories
    }
  }
    
var flt = 2;
const width = Dimensions.get('window').width;
class SelectImage extends Component {
    constructor(props) {
        super(props);
       this.state = {
            fileList: [],
            
        }
       // this.onSelectedImage = this.onSelectedImage.bind(this);

    }
   

   onSelectedImage = (image) => {
    let newDataImg = this.state.fileList;
    for (let i = 0; i < Object.keys(image).length; i++)  {
        let imageData = image[String(i)];
        const source = {uri: imageData.path};
        let item = {
            id: String(i),
            url: source,
            content: imageData.data
        };
        newDataImg.push(item);
        this.setState({fileList: newDataImg})
    }

        
    }

    choosePhotoFromLibrary = () => {
        ImagePicker.openPicker({
            compressImageMaxWidth: 400,
            compressImageMaxHeight: 400,
            cropping: true,
            compressImageQuality: 0.7,
            multiple: true,
          }).then(image => {
            this.onSelectedImage(image);
            console.log(image);
          });
    }

    takePhotoFromCamera = () => {
        ImagePicker.openCamera({
            compressImageMaxWidth: 400,
            compressImageMaxHeight: 400,
            cropping: true,
            compressImageQuality: 0.7
          }).then(image => {
            this.onSelectedImage2(image);
            console.log(image);
          });
    }

    onClickAddImage = () => {
        const BUTTONS = ['Take Photo', 'Choose Photo Library', 'Cancel'];
        ActionSheet.show({
                options: BUTTONS,
                cancelButtonIndex: 2,
                title: 'Select a photo'},
            buttonIndex => {
                switch(buttonIndex) {
                    case 0:
                        this.takePhotoFromCamera();
                        break;
                    case 1:
                        this.choosePhotoFromLibrary();
                        break;
                    default:
                        break;
                }
            })
    }

    renderItem = ({item,index}) => {
        return (
            <View>
                <Card>
                    <Image source={item.url} style={styles.itemImage} />
                    <Text>{item.id}</Text>
                </Card>
            </View>
        )
    }

    render() {
        let {fileList} = this.state;
       // const pic = this.props.navigation.route.params('pic',"");
        //const pic2 = this.props.navigation.route.params('pic2',"");
        return(
            <Root>
                <View style={styles.container}>
                
                
                    <FlatList
                        data={fileList}
                        numColumns={flt}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item.id.toString()}
                        extraData={this.state}
                        />
                    <TouchableOpacity onPress={this.onClickAddImage} style={styles.btnPressStyle}>
                        <Text style={styles.txtStyle}>Press Add Image</Text>
                    </TouchableOpacity>
                </View>                
            </Root>
        );
    }


}const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 50,
     paddingLeft: 30,
     paddingRight: 30,
     marginBottom: 30
     
    },
    btnPressStyle: {
      backgroundColor: '#0080ff',
      height: 50,
      width: width -60,
      alignItems:'center',
      justifyContent: 'center'
    },
    txtStyle : {
     color: '#ffffff'
    
    },
    itemImage: {
        backgroundColor: '#2F455C',
        height:150,
       width: 150,
        //width: width - 60,
        borderRadius: 8,
       resizeMode: 'contain',
      // padding: 10
    }
    })
    
    export default connect(mapStateToProps)(SelectImage);
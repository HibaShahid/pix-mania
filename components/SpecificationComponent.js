import React, {Component} from "react";
import { Text, View, ScrollView , Dimensions, Image,StyleSheet, TouchableOpacity, Button} from 'react-native';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import {ButtonGroup,CheckBox,Icon} from 'react-native-elements'

const mapStateToProps = state => {
    return {
      categories: state.categories,
      frames : state.frames
    }
  }

  const  width  = Dimensions.get("window").width;
  const height = width * 0.6; 

class SpecificationComponent extends Component {
    constructor () {
        super()
        this.state = {
          selectedIndex: 1,
          selectedIndex1: 0,
          active: 0,
          size:'8*8',
      showMe: false,
      checked: 'true'
        }
        this.updateIndex = this.updateIndex.bind(this)
        this.updateIndex1 = this.updateIndex1.bind(this)
      }
   
      updateIndex (selectedIndex) {
            
            this.setState({selectedIndex})
        
      }
      updateIndex1 (selectedIndex1) {
        this.setState({selectedIndex1})
      }
   /* state ={
      active: 0,
      showMe: false
    }*/
    opertation(){
        this.setState({
          showMe: !this.state.showMe
        })
      }
      change = ({nativeEvent}) => {
        const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.wigth);
        if(slide !== this.state.active){
          this.setState({active: slide});
        }  
      }
      renderImage(selectedIndex) {
        
        if(selectedIndex==0) {
            return(
                console.log("select0")
             //this.props.categories.categories
            )
        }
        else 
        if (selectedIndex==1) {
            return(
                console.log("select1")
                //this.props.frames.frames
            )
        }
        else 
        if (selectedIndex==2) {
            return(
                console.log("select2")
               // this.props.categories.categories
            )
        }
      }
render() {
    //this.renderImage(selectedIndex);
    const buttons1 = ['6*6"', '8*8"', '10*10"']
    const buttons2 = ['SOFT COVER', 'HARD COVER']
    const { selectedIndex } = this.state
    const { selectedIndex1 } = this.state
    const item = this.props.categories.categories;
    //const item = this.renderImage(selectedIndex)
    return (
      <View style={{flex:1,backgroundColor: "#fff"}}>
     
      <View style={styles.container}>
        <ScrollView 
        pagingEnabled 
        horizontal 
        onScroll={this.change}
        style={styles.scroll}
        showsHorizontalScrollIndicator={false}
        >
            {
              item.map((item,index) => (
                <Image
                  key={index}
                  source={{uri: baseUrl + item.image}}
                  style = {styles.image}
                />
              ))
            } 
  
          </ScrollView>
          <View style={styles.pagination}>
        {
          item.map((i,k) => (
            <Text key = {k} style={k==this.state.active ? styles.pagingActiveText : styles.pagingText}>⬤</Text>
          )

          )
        }
        </View>
          </View>
          <View style={{flexDirection:'column' ,marginTop: 10}}>
          
              <Text style={{fontSize: 25, textAlign:'left'}}>Options                    starting at $20</Text>
              <Text style={{fontSize: 20,color:'grey',textAlign:'right',marginVertical:0}}>30Pages</Text>
            
          </View> 
          
          <View> 
          <ButtonGroup
          onPress={this.updateIndex}
          selectedIndex={selectedIndex}
            buttons={buttons1}
            containerStyle={{height: 50}}
            />
            <Text style={{marginTop: 10, fontSize: 20, color:'grey', marginBottom: 10}}>COVER TYPE</Text>
          </View> 
          <View>
          <ButtonGroup
          onPress={this.updateIndex1}
          selectedIndex={selectedIndex1}
            buttons={buttons2}
            containerStyle={{height: 50}}
            />
            <Text style={{marginTop: 10, fontSize: 20, color:'grey', marginBottom: 10}}>BOOK COLOR</Text>
        </View>
        <View style={{flexDirection:'row'}}>
       {/* <CheckBox
                center
                title='Click Here'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
               // checked={false}
                containerStyle={{borderRadius: 10}}
                />
                <CheckBox
                center
                title='Click Here'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                //checked={this.state.checked}
                containerStyle={{borderRadius: 10,}}
                />
             
                <CheckBox
                center
                title='Click Here'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
               // checked={this.state.checked}
                containerStyle={{borderRadius: 10}}
       />*/}
        </View>
        <Button
      
        title='Create' 
        onPress={() => this.props.navigation.navigate('SelectImage',{pic: this.state.selectedIndex, pic2: this.state.selectedIndex1}) }
       // onPress={() => alert('hello')}
       // onPress={() => this.props.navigation.navigate('PickImage') }
       />
         </View>
     )}
    }

 export default connect(mapStateToProps)(SpecificationComponent);  


 const styles = StyleSheet.create({
    container: {
      //flex:1,
      marginTop: 0, 
      width,
       height,backgroundColor:'pink'
    },
    scroll: { 
      width,
       height
    },
      
    image: {
      width,
       height,
      resizeMode: 'cover'
    },
    pagination: {
        flexDirection:'row',
        position:'absolute',
        bottom:0,
        alignSelf:'center',
      },
  });
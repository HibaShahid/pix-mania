import React, {Component,  useState } from 'react';
import {StyleSheet, View, Text , FlatList, TouchableOpacity, Image, Modal, TouchableHighlight} from 'react-native';
import { Card } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';



const mapStateToProps = state => {
    return {
      frames : state.frames
    }
  }

  var flatCol = 2;


class PhotoBook extends Component {

    render() {
        
        const createModel = () => {
            const [modalVisible, setModalVisible] = useState(false);
            return(
                <View style={styles.centeredView}>
                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={modalVisible}
                  onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                  }}
                >
                  <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                      <Text style={styles.modalText}>Hello World!</Text>
          
                      <TouchableHighlight
                        style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                        onPress={() => {
                          setModalVisible(!modalVisible);
                        }}
                      >
                        <Text style={styles.textStyle}>Hide Modal</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </Modal>
          
                <TouchableHighlight
                  style={styles.openButton}
                  onPress={() => {
                    setModalVisible(true);
                  }}
                >
                  <Text style={styles.textStyle}>Show Modal</Text>
                </TouchableHighlight>
              </View>
            )
        }
        const renderFramesItem = ({item, index}) => {
            return(
              
              <TouchableOpacity 
                style={styles.cardContainer} 
                onPress={() => this.props.navigation.navigate('PhotoBookDetails')  }
                >
                <Card
                containerStyle={styles.card}
                key = {index}
               image = {{uri: baseUrl + item.image}}
      
                >
                <Text style={styles.cardText}>{item.name}</Text>
                </Card>
                </TouchableOpacity>
            );
          };
        return(
            <View style={styles.container}>
                    <View style={styles.containerA}>
                        <Image
                            style={styles.containerImage}
                            source={require('./images/promotion.jpg')}
                        />
                        <View>
                            <Text style={styles.allheadingtext}>Categories</Text>
                        </View>  
                        <View style={styles.containerB}>
                            <FlatList
                            numColumns = {flatCol} 
                            data={this.props.frames.frames}
                            renderItem={renderFramesItem}
                            keyExtractor={item => item.id.toString()}
                            />
                        </View>
                    </View>  
            </View>
        )
    }
}

export default connect(mapStateToProps)(PhotoBook);
const styles = StyleSheet.create({
    container: {
      flex: 1,
        backgroundColor: '#fff'
    },
    containerA:{
        flex:1,
        marginTop: 20
    },
    containerB:{
        flex:1,
        marginTop: 20
    },
    containerImage: {
        width: '100%',
        height: '30%' 
    },
    cardContainer:{
        flex: 1,
        alignItems: 'center'
    },
    allheadingtext:{
        fontSize: 25,
        marginTop: 10,
        fontWeight: 'bold',
        paddingLeft: 10
    },
    card : {
        borderWidth: 0,
        elevation: 0,
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0, //default is 1
        shadowRadius: 0,
        width: '50%',
        height: '50%'
      
      }

    })
    
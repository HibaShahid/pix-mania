import * as ActionTypes from './ActionTypes';

export const frames = (state = { isLoading: true,
    errMess: null,
    frames:[]}, action) => {
switch (action.type) {
case ActionTypes.ADD_FRAMES:
return {...state, isLoading: false, errMess: null, frames: action.payload};

case ActionTypes.FRAMES_LOADING:
return {...state, isLoading: true, errMess: null, frames: []}

case ActionTypes.FRAMES_FAILED:
return {...state, isLoading: false, errMess: action.payload};

default:
return state;
}
};

import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';


export const fetchCategories = () => (dispatch) => {

    dispatch(categoriesLoading());

    return fetch(baseUrl + 'categories')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(categories => dispatch(addCategories(categories)))
    .catch(error => dispatch(categoriesFailed(error.message)));
};

export const categoriesLoading = () => ({
    type: ActionTypes.CATEGORIES_LOADING
});

export const categoriesFailed = (errmess) => ({
    type: ActionTypes.CATEGORIES_FAILED,
    payload: errmess
});

export const addCategories = (categories) => ({
    type: ActionTypes.ADD_CATEGORIES,
    payload: categories
});


export const fetchFrames = () => (dispatch) => {

    dispatch(framesLoading());

    return fetch(baseUrl + 'frames')
    .then(response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response => response.json())
    .then(frames => dispatch(addFrames(frames)))
    .catch(error => dispatch(framesFailed(error.message)));
};

export const framesLoading = () => ({
    type: ActionTypes.FRAMES_LOADING
});

export const framesFailed = (errmess) => ({
    type: ActionTypes.FRAMES_FAILED,
    payload: errmess
});

export const addFrames = (frames) => ({
    type: ActionTypes.ADD_FRAMES,
    payload: frames
});


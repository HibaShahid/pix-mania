import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { categories } from './categories';
import {frames} from './frames';

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            categories,
            frames
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}
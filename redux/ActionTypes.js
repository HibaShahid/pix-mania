export const CATEGORIES_LOADING = 'CATEGORIES_LOADING';
export const ADD_CATEGORIES = 'ADD_CATEGORIES';
export const CATEGORIES_FAILED = 'CATAGORIES_FAILED';
export const FRAMES_LOADING = 'FRAMES_LOADING';
export const ADD_FRAMES = 'ADD_FRAMES';
export const FRAMES_FAILED = 'FRAMES_FAILED';
export const PRINTLIST_LOADING = 'PRINTLIST_LOADING';
export const ADD_PRINTLIST = 'ADD_PRINTLIST';
export const PRINTLIST_FAILED = 'PRINTLIST_FAILED';
import * as ActionTypes from './ActionTypes';

export const printlist = (state = { isLoading: true,
                                 errMess: null,
                                 printlist:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_PRINTLIST:
            return {...state, isLoading: false, errMess: null, printlist: action.payload};

        case ActionTypes.PRINTLIST_LOADING:
            return {...state, isLoading: true, errMess: null, printlist: []}

        case ActionTypes.PRINTLIST_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
          return state;
      }
};